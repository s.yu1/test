# -*- coding: utf-8 -*-
"""Dialog management of mirror sign up"""

import random
import csv
import logging
import requests
from time import time
from threading import Thread

from ask_sdk_core.skill_builder import SkillBuilder
from ask_sdk_core.dispatch_components import (
    AbstractRequestHandler, AbstractExceptionHandler,
    AbstractRequestInterceptor, AbstractResponseInterceptor)
from ask_sdk_core.utils import is_request_type, is_intent_name
from ask_sdk_core.handler_input import HandlerInput
from ask_sdk_core.utils import get_slot_value

from ask_sdk_model.ui import SimpleCard
from ask_sdk_model import Response

SKILL_NAME = "Show Up"

# Alexa voice
voice = "<voice name=\"Matthew\">"

# Utterance per state
LAUNCHED = voice + "<p> Hello, I'm your smart mirror. </p> <p> What's your name? </p></voice>"
SHOWUP_FAILED = voice + "<p> Sorry </p> <p> What's your name? </p></voice>"
#SHOWUP_DONE = voice + "<p> Hi firstname </p><p> may I register you? </p></voice>"
REGISTRATION_STARTED = voice + "<p> Nice. </p> <p> Ok, move  a little your face, in way I can known you better. </p> <p> When you finish say ok </p></voice> "
REGISTRATION_COMPLETED = voice + "<p> Good job </p> <p> This is your personal page </p> <p> If you want to change something go to the app </p></voice> "
ABORTED = voice + "<p> Registration aborted </p></voice> "
REGISTRATION_DENIED = voice + "<p> Ok, if you need me I'm here </p></voice>"
UNKNOWN_STATE = voice + "<p> I don't know how we came here </p></voice>"
FAILED = voice + "<p> Too much retry </p><p> better talk later </p></voice>"

sb = SkillBuilder()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# Set this to True if mirror is running
SHOULD_SEND_DIALOG_STATE = True

# Set this to True if visual server is running
SHOULD_SEND_USER_FIRSTNAME = True

# Max number of retry before exit
MAX_FAIL = 1

# Names accepted
csv_list = csv.reader(open('list_of_names.csv', newline=''))
list_of_names = list(csv_list)


# Utility functions

def send_dialog_state(state, phrase):
    if SHOULD_SEND_DIALOG_STATE:
        data = {"dialog_state": state, "phrase": phrase}
        url = "http://149.132.178.175:7007"
        resp = requests.put(url+"/DialogState", json = data)
        logger.info("Dialog state and phrase sended")
    else:
        logger.info("Dialog state sending  and phrase disabled")

def send_user_firstname(firstname):
    if SHOULD_SEND_USER_FIRSTNAME:
        data = {"name": firstname, "dialog_state": "showUp"}
        url = "http://149.132.178.179:8888"
        resp = requests.put(url+"/User", json = data)
        logger.info("User firstname sended")
    else:
        logger.info("User firstname sending disabled")
        
def incr_and_check_fail_counter(session_attr):
    failed = False
    session_attr["fail_counter"] += 1
    if session_attr["fail_counter"] > MAX_FAIL:
        failed = True
    return failed

# Built-in Intent Handlers

class LaunchRequestHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_request_type("LaunchRequest")(handler_input)
        
    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("Start launch handler...")
        
        session_attr = handler_input.attributes_manager.session_attributes
        session_attr["dialog_state"] = "show_up_launched"
        session_attr["fail_counter"] = 0
        session_attr["user_firstname"] = "undefined"
        
        speech_text = LAUNCHED
        reprompt = LAUNCHED
        
        should_end_session = False
        
        handler_input.response_builder.speak(speech_text).ask(reprompt).set_should_end_session(should_end_session)
        
        send_dialog_state("show_up_launched",speech_text)
        
        logger.info("Dialog state: %s", session_attr["dialog_state"])
        logger.info("Fail counter: %d", session_attr["fail_counter"])
        logger.info("User firstname: %s", session_attr["user_firstname"])
        
        logger.info("..Finish launch handler")
        return handler_input.response_builder.response
        
class ShowUpHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        #logger.info("Intent showup? : %r", is_intent_name("ShowUp")(handler_input))
        return is_intent_name("ShowUp")(handler_input)
    
    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        
        logger.info("Start ShowUp handler...")
        
        session_attr = handler_input.attributes_manager.session_attributes
        
        firstname = get_slot_value(handler_input,"FirstName")
        firstname_ok = False
        # Aggiungere controllo che nome sia in list_of_names
        if any(firstname in s for s in list_of_names):
            firstname_ok = True
        
        should_end_session = False
        failed = False
        
        if session_attr["dialog_state"] == "show_up_launched" or session_attr["dialog_state"] == "show_up_failed":
            if firstname_ok:
                session_attr["dialog_state"] = "show_up_done"
                session_attr["fail_counter"] = 0
                session_attr["user_firstname"] = firstname
                send_user_firstname(firstname)
                speech_text = voice + "<p> Hi "+firstname+" </p><p> may I register you? </p></voice>"
                reprompt = voice + "<p> Hi "+firstname+" </p><p> may I register you? </p></voice>"
                should_end_session = False
                handler_input.response_builder.speak(speech_text).ask(reprompt).set_should_end_session(should_end_session)
            else:
                failed = incr_and_check_fail_counter(session_attr)
                if failed:
                    logger.info("Max retry reached")
                    session_attr["dialog_state"] = "show_up_failed"
                    should_end_session = True
                    speech_text = FAILED
                    handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
                else:
                    session_attr["dialog_state"] = "show_up_failed"
                    # <p><audio src=\"soundbank://soundlibrary/human/amzn_sfx_laughter_01\"/></p>
                    speech_text = voice + "<p><audio src=\"soundbank://soundlibrary/human/amzn_sfx_laughter_01\"/></p><p> I don't think your name is "+firstname+" </p><p> Be serious, what's your name? </p></voice>"
                    should_end_session = False
                    handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        elif session_attr["dialog_state"] == "show_up_done":
            failed = incr_and_check_fail_counter(session_attr)
            if failed:
                logger.info("Max retry reached")
                session_attr["dialog_state"] = "show_up_failed"
                should_end_session = True
                speech_text = FAILED
                handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
            else:
                if firstname_ok:
                    session_attr["dialog_state"] = "show_up_done"
                    session_attr["user_firstname"] = firstname
                    send_user_firstname(firstname)
                    # <p><audio src=\"soundbank://soundlibrary/human/amzn_sfx_laughter_01\"/></p>
                    speech_text = voice + "<p> Hi "+firstname+" </p><p> may I register you? </p></voice>"
                    should_end_session = False
                    handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
                else:
                    session_attr["dialog_state"] = "show_up_failed"
                    speech_text = voice + "<p><audio src=\"soundbank://soundlibrary/human/amzn_sfx_laughter_01\"/></p><p> I don't think your name is "+firstname+" </p><p> Be serious, what's your name? </p></voice>"
                    should_end_session = False
                    handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        elif session_attr["dialog_state"] == "show_up_registration_started":
            session_attr["dialog_state"] = "show_up_registration_completed"
            session_attr["fail_counter"] += 1
            speech_text = REGISTRATION_COMPLETED
            should_end_session = True
            handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        else:
            logger.info("Unknown state: %s", session_attr["dialog_state"])
            session_attr["dialog_state"] = "show_up_unknown_state"
            speech_text = UNKNOWN_STATE
            should_end_session = True
            handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)

        
        send_dialog_state(session_attr["dialog_state"],speech_text)
        
        logger.info("Dialog state: %s", session_attr["dialog_state"])
        logger.info("Fail counter: %d", session_attr["fail_counter"])
        logger.info("User firstname: %s", session_attr["user_firstname"])
        logger.info("..Finish ShowUp handler")
        return handler_input.response_builder.response
        
class YesHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name("Yes")(handler_input)
        
    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("Start Yes handler...")
        session_attr = handler_input.attributes_manager.session_attributes
        
        should_end_session = False
        
        if session_attr["dialog_state"] == "show_up_launched" or session_attr["dialog_state"] == "show_up_failed":
            failed = incr_and_check_fail_counter(session_attr)
            if failed:
                logger.info("Max retry reached")
                session_attr["dialog_state"] = "show_up_failed"
                should_end_session = True
                speech_text = FAILED
                handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
            else:
                session_attr["dialog_state"] = "show_up_failed"
                speech_text = SHOWUP_FAILED
                reprompt = SHOWUP_FAILED
                should_end_session = False
                handler_input.response_builder.speak(speech_text).ask(reprompt).set_should_end_session(should_end_session)
        elif session_attr["dialog_state"] == "show_up_done":
            session_attr["dialog_state"] = "show_up_registration_started"
            session_attr["fail_counter"] = 0
            speech_text = REGISTRATION_STARTED
            should_end_session = False
            handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        elif session_attr["dialog_state"] == "show_up_registration_started":
            session_attr["dialog_state"] = "show_up_registration_completed"
            session_attr["fail_counter"] = 0
            speech_text = REGISTRATION_COMPLETED
            should_end_session = True
            handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        else:
            logger.info("Unknown state: %s", session_attr["dialog_state"])
            session_attr["dialog_state"] = "show_up_unknown_state"
            speech_text = UNKNOWN_STATE
            should_end_session = True
            handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
            
        
        send_dialog_state(session_attr["dialog_state"],speech_text)
        
        logger.info("Dialog state: %s", session_attr["dialog_state"])
        logger.info("Fail counter: %d", session_attr["fail_counter"])
        logger.info("User firstname: %s", session_attr["user_firstname"])
        logger.info("..Finish Yes handler")
        return handler_input.response_builder.response
        
class NoHandler(AbstractRequestHandler):
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_intent_name("No")(handler_input)
        
    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("Start No handler...")
        session_attr = handler_input.attributes_manager.session_attributes
        
        should_end_session = False
        
        if session_attr["dialog_state"] == "show_up_launched" or session_attr["dialog_state"] == "show_up_failed":
            failed = incr_and_check_fail_counter(session_attr)
            if failed:
                logger.info("Max retry reached")
                session_attr["dialog_state"] = "show_up_aborted"
                should_end_session = True
                speech_text = FAILED
                handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
            else:
                session_attr["dialog_state"] = "show_up_aborted"
                speech_text = SHOWUP_FAILED
                reprompt = SHOWUP_FAILED
                should_end_session = False
                handler_input.response_builder.speak(speech_text).ask(reprompt).set_should_end_session(should_end_session)
        elif session_attr["dialog_state"] == "show_up_done":
            session_attr["dialog_state"] = "show_up_registration_denied"
            session_attr["fail_counter"] = 0
            speech_text = REGISTRATION_DENIED
            should_end_session = True
            handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        elif session_attr["dialog_state"] == "show_up_registration_started":
            session_attr["dialog_state"] = "show_up_registration_completed"
            session_attr["fail_counter"] += 1
            speech_text = REGISTRATION_COMPLETED
            should_end_session = True
            handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        else:
            logger.info("Unknown state: %s", session_attr["dialog_state"])
            session_attr["dialog_state"] = "show_up_unknown_state"
            speech_text = UNKNOWN_STATE
            should_end_session = True
            handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        
        send_dialog_state(session_attr["dialog_state"],speech_text)
        
        logger.info("Dialog state: %s", session_attr["dialog_state"])
        logger.info("Fail counter: %d", session_attr["fail_counter"])
        logger.info("User firstname: %s", session_attr["user_firstname"])
        logger.info("..Finish No handler")
        return handler_input.response_builder.response

class CancelOrStopIntentHandler(AbstractRequestHandler):
    """Single handler for Cancel and Stop Intent."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return (is_intent_name("AMAZON.CancelIntent")(handler_input) or
                is_intent_name("AMAZON.StopIntent")(handler_input))

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("Start Cancel or Stop handler...")
        session_attr = handler_input.attributes_manager.session_attributes
        
        speech_text = ABORTED
        should_end_session = True
        handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        
        session_attr["dialog_state"] = "show_up_aborted"
        send_dialog_state(session_attr["dialog_state"],speech_text)
        
        logger.info("Dialog state: %s", session_attr["dialog_state"])
        logger.info("Fail counter: %d", session_attr["fail_counter"])
        logger.info("User firstname: %s", session_attr["user_firstname"])
        logger.info("..Finish Cancel and Stop handler")
        return handler_input.response_builder.response


class FallbackIntentHandler(AbstractRequestHandler):
    """Handler for Fallback Intent.
    """
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return ( is_intent_name("AMAZON.FallbackIntent")(handler_input) or is_intent_name("AMAZON.HelpIntent")(handler_input) )

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("Start Fallback or Help handler...")
        session_attr = handler_input.attributes_manager.session_attributes
        
        should_end_session = False
        
        if session_attr["dialog_state"] == "show_up_launched" or session_attr["dialog_state"] == "show_up_failed":
            failed = incr_and_check_fail_counter(session_attr)
            if failed:
                logger.info("Max retry reached")
                session_attr["dialog_state"] = "show_up_failed"
                should_end_session = True
                speech_text = FAILED
                handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
            else:
                session_attr["dialog_state"] = "show_up_failed"
                speech_text = SHOWUP_FAILED
                should_end_session = False
                handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        elif session_attr["dialog_state"] == "show_up_done":
            failed = incr_and_check_fail_counter(session_attr)
            if failed:
                logger.info("Max retry reached")
                session_attr["dialog_state"] = "show_up_failed"
                should_end_session = True
                speech_text = FAILED
                handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
            else:
                session_attr["dialog_state"] = "show_up_done"
                speech_text = voice + "<p> Hi "+session_attr["user_firstname"]+" </p><p> may I register you? </p></voice>"
                should_end_session = False
                handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        elif session_attr["dialog_state"] == "show_up_registration_started":
            session_attr["dialog_state"] = "show_up_registration_completed"
            session_attr["fail_counter"] += 1
            speech_text = REGISTRATION_COMPLETED
            should_end_session = True
            handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        else:
            logger.info("Unknown state: %s", session_attr["dialog_state"])
            session_attr["dialog_state"] = "show_up_unknown_state"
            speech_text = UNKNOWN_STATE
            should_end_session = True
            handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        
        send_dialog_state(session_attr["dialog_state"],speech_text)
        
        logger.info("Dialog state: %s", session_attr["dialog_state"])
        logger.info("Fail counter: %d", session_attr["fail_counter"])
        logger.info("User firstname: %s", session_attr["user_firstname"])
        logger.info("..Finish Fallback or Help handler")
        return handler_input.response_builder.response


class SessionEndedRequestHandler(AbstractRequestHandler):
    """Handler for Session End."""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_request_type("SessionEndedRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        logger.info("In SessionEndedRequestHandler")

        logger.info("Session ended reason: {}".format(
            handler_input.request_envelope.request.reason))
        
        if "{}".format(handler_input.request_envelope.request.reason) == "SessionEndedReason.EXCEEDED_MAX_REPROMPTS":
            speech_text = voice + "<p> I'm not hearing you. Check the microphone status and retry later</p></voice>"
            handler_input.response_builder.speak(speech_text)
            
        session_attr = handler_input.attributes_manager.session_attributes
        
        speech_text = ABORTED
        should_end_session = True
        handler_input.response_builder.speak(speech_text).set_should_end_session(should_end_session)
        
        session_attr["dialog_state"] = "show_up_aborted"
        send_dialog_state(session_attr["dialog_state"],speech_text)
        
        logger.info("Dialog state: %s", session_attr["dialog_state"])
        logger.info("Fail counter: %d", session_attr["fail_counter"])
        logger.info("User firstname: %s", session_attr["user_firstname"])
        logger.info("..Finish Session ended handler")

       
       
        
        return handler_input.response_builder.response


# Exception Handler
class CatchAllExceptionHandler(AbstractExceptionHandler):
    """Catch all exception handler, log exception and
    respond with custom message.
    """
    def can_handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> bool
        return True

    def handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> Response
        logger.info("In CatchAllExceptionHandler")
        
        return handler_input.response_builder.response


# Request and Response loggers
class RequestLogger(AbstractRequestInterceptor):
    """Log the alexa requests."""
    def process(self, handler_input):
        # type: (HandlerInput) -> None
        logger.debug("Alexa Request: {}".format(
            handler_input.request_envelope.request))


class ResponseLogger(AbstractResponseInterceptor):
    """Log the alexa responses."""
    def process(self, handler_input, response):
        # type: (HandlerInput, Response) -> None
        logger.debug("Alexa Response: {}".format(response))


# Register intent handlers
sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(ShowUpHandler())
sb.add_request_handler(YesHandler())
sb.add_request_handler(NoHandler())
#sb.add_request_handler(HelpIntentHandler())
sb.add_request_handler(CancelOrStopIntentHandler())
sb.add_request_handler(FallbackIntentHandler())
sb.add_request_handler(SessionEndedRequestHandler())

# Register exception handlers
sb.add_exception_handler(CatchAllExceptionHandler())

# TODO: Uncomment the following lines of code for request, response logs.
#sb.add_global_request_interceptor(RequestLogger())
#sb.add_global_response_interceptor(ResponseLogger())

# Handler name that is used on AWS lambda
lambda_handler = sb.lambda_handler()
